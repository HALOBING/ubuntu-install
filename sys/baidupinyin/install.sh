#!/bin/bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
sudo apt-get install -y aptitude
sudo aptitude install -y fcitx-bin fcitx-table fcitx-config-gtk fcitx-config-gtk2 fcitx-frontend-all
sudo aptitude install -y qt5-default qtcreator qml-module-qtquick-controls2

sudo dpkg -i $DIR/fcitx-baidupinyin.deb
sudo apt-get -f install -y