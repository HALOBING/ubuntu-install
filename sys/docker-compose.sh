#!/bin/bash

# sudo wget https://github.com/docker/compose/releases/download/v2.14.2/docker-compose-`uname -s`-`uname -m` -O /usr/local/bin/docker-compose
sudo wget https://gitlab.com/fuyb999/docker-compose/-/raw/main/v2.20.3/docker-compose-`uname -s | tr '[:upper:]' '[:lower:]'`-`uname -m` -O /usr/local/bin/docker-compose
sudo chmod +x /usr/local/bin/docker-compose

#sudo curl -L "https://github.com/docker/compose/releases/download/v2.24.1/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
# 如果发现速度过慢，可以尝试使用下面的国内源
#sudo curl -SL https://mirror.ghproxy.com/https://github.com/docker/compose/releases/download/v2.24.1/docker-compose-`uname -s`-`uname -m` -o /usr/local/bin/docker-compose
