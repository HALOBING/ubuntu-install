# Ubuntu 20.04: Go to Settings > Keyboard and press the '+' button at the bottom.
# Name the command as you like it, e.g. `flameshot`. And in the command insert `/usr/bin/flameshot gui`
# Then click "Set Shortcut.." and press `Ctrl+Alt+A`. 

sudo apt-get install -y flameshot

# 设置快捷键的命令
command="/usr/bin/flameshot gui"
# 自定义的快捷键
shortcut="Ctrl+Alt+A"

# 添加自定义快捷键
gsettings set org.gnome.settings-daemon.plugins.media-keys.custom-keybinding:/org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom0/ name "Flameshot"
gsettings set org.gnome.settings-daemon.plugins.media-keys.custom-keybinding:/org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom0/ command "$command"
gsettings set org.gnome.settings-daemon.plugins.media-keys.custom-keybinding:/org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom0/ binding "$shortcut"

# 启用自定义快捷键
gsettings set org.gnome.settings-daemon.plugins.media-keys custom-keybindings "['/org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom0/']"