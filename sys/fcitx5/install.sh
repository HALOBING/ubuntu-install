#!/bin/bash

apt-get install -y gnome-tweaks fcitx5-* && \
        apt-get purge -y fcitx5-module-emoji fcitx5-frontend-* fcitx5-keyman fcitx5-sayura fcitx5-anthy fcitx5-chewing fcitx5-hangul fcitx5-kkc fcitx5-m17n fcitx5-mozc fcitx5-rime fcitx5-skk fcitx5-unikey fcitx5-module-lua-* fcitx5-module-pinyinhelper-dev fcitx5-module-punctuation-dev fcitx5-modules-dev && \
        apt-get autoremove -y

if [ ! -f /etc/profile.d/fcitx5.sh ]; then
  tee /etc/profile.d/fcitx5.sh <<EOF
export XMODIFIERS=@im=fcitx5
export GTK_IM_MODULE=fcitx5
export QT_IM_MODULE=fcitx5
EOF
  tee ~/.config/fcitx5/profile <<EOF
[Groups/0]
# Group Name
Name=Default
# Layout
Default Layout=us
# Default Input Method
DefaultIM=pinyin

[Groups/0/Items/0]
# Name
Name=keyboard-us
# Layout
Layout=

[Groups/0/Items/1]
# Name
Name=pinyin
# Layout
Layout=

[GroupOrder]
0=Default

EOF

fi

