#!/bin/bash

sudo dpkg --configure -a
sudo apt-get install -y apt-transport-https ca-certificates curl software-properties-common

# sudo curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
# sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"

sudo curl -fsSL http://mirrors.aliyun.com/docker-ce/linux/ubuntu/gpg | sudo apt-key add -

#if [ `grep -c "mirrors.aliyun.com/docker-ce/linux/ubuntu" /etc/apt/sources.list` -ne '2' ];then
    #sudo add-apt-repository "deb [arch=amd64] http://mirrors.aliyun.com/docker-ce/linux/ubuntu $(lsb_release -cs) stable"
#fi

sudo echo "deb [arch=amd64] http://mirrors.aliyun.com/docker-ce/linux/ubuntu $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker-ce.list

sudo apt-get update
sudo apt-get install -y docker-ce

sudo systemctl start docker
sudo systemctl enable docker

sudo tee /etc/docker/daemon.json <<EOF
{
  "registry-mirrors": [
    "https://dockerhub.icu",
    "https://docker.mirrors.ustc.edu.cn",
    "https://registry.docker-cn.com"
  ],
  "data-root": "/home/docker/lib/docker"
}
EOF

sudo systemctl daemon-reload
sudo systemctl restart docker

sudo docker -v

if [ -d "/home/docker" ];then
  echo -e "\033[31m skip docker. \033[0m"
else
  # 迁移 docker 到 home
  sudo rm -rf /home/docker
  sudo systemctl stop docker
  sudo mkdir -p /home/docker/lib
  sudo rsync -avzP /var/lib/docker /home/docker/lib/
  # 19.xx 后 dockerd --graph 已失效 在/etc/docker/daemon.json中配置data-root
  #sudo mkdir -p /etc/systemd/system/docker.service.d/
  #sudo tee /etc/systemd/system/docker.service.d/devicemapper.conf <<EOF
#[Service]
#ExecStart=
#ExecStart=/usr/bin/dockerd --graph=/home/docker/lib/docker
#EOF

  sudo systemctl daemon-reload
  sudo systemctl restart docker
  sudo rm -rf /var/lib/docker
fi

sudo docker info