#!/bin/bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

# remove libreoffice
which libreoffice > /dev/null 2>&1
if [ "$?" -eq "0" ]; then
  sudo apt-get remove --purge libreoffice* -y
  sudo apt-get autoremove -y
  sudo apt-get autoclean -y
  sudo apt-get clean -y
  rm -rf $WORK_DIR/.config/libreoffice
fi

deb="wps-office_11.1.0.10920_amd64.deb"

wget -nc --no-check-certificate https://wps-linux-personal.wpscdn.cn/wps/download/ep/Linux2019/10920/$deb \
    -O $WORK_DIR/$deb
    
sudo dpkg -i $WORK_DIR/$deb

cd $DIR/ttf-wps-fonts && sudo sh install.sh && cd -


# E: The package wps-office needs to be reinstalled, but I can't find an archive for it.
# sudo dpkg --remove --force-remove-reinstreq wps-offic