#!/bin/bash

sudo apt-get install -y libgl1-mesa-glx libegl1-mesa libxrandr2 libxrandr2 libxss1 libxcursor1 libxcomposite1 libasound2 libxi6 libxtst6


tee $WORK_DIR/.condarc << EOF
envs_dirs:
  - /home/${USER_NAME}/.conda/envs
pkgs_dirs:
  - /home/${USER_NAME}/.conda/pkgs
auto_activate_base: true
show_channel_urls: true
channels:
  - https://mirrors.tuna.tsinghua.edu.cn/anaconda/pkgs/main/
  - https://mirrors.tuna.tsinghua.edu.cn/anaconda/pkgs/free/
  - defaults
EOF

chmod 777 $WORK_DIR/.condarc

wget -nc https://repo.anaconda.com/archive/Anaconda3-2024.06-1-Linux-x86_64.sh -O $WORK_DIR/Anaconda3-2024.06-1-Linux-x86_64.sh
bash $WORK_DIR/Anaconda3-2024.06-1-Linux-x86_64.sh -b -p $WORK_DIR/anaconda3 -f

chmod -R 777 $WORK_DIR/anaconda3


ANACONDA_HOME=$WORK_DIR/anaconda3/bin

#conda config --add channels https://mirrors.tuna.tsinghua.edu.cn/anaconda/pkgs/free/
#conda config --add channels https://mirrors.tuna.tsinghua.edu.cn/anaconda/pkgs/main/
#conda config --set show_channel_urls yes
#conda config --set auto_activate_base true

sudo -iu $USER_NAME $ANACONDA_HOME/conda init bash
sudo -iu $USER_NAME source $WORK_DIR/.bashrc
