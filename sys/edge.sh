#!/bin/bash

wget -nc https://packages.microsoft.com/repos/edge/pool/main/m/microsoft-edge-stable/microsoft-edge-stable_117.0.2045.35-1_amd64.deb?brand=M102 -O $WORK_DIR/microsoft-edge-stable_117.0.2045.35-1_amd64.deb

dpkg -i $WORK_DIR/microsoft-edge-stable_117.0.2045.35-1_amd64.deb
