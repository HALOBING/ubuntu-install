#!/bin/bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
sudo dpkg --force-depends -i $DIR/openvpn3_20_focal_amd64.deb
sudo apt-get -f install -y