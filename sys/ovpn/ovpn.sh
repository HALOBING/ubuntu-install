#!/bin/bash

# from repo need /etc/hosts
curl -fsSL https://swupdate.openvpn.net/repos/openvpn-repo-pkg-key.pub | gpg --dearmor > openvpn-repo-pkg-keyring.gpg
sudo mv openvpn-repo-pkg-keyring.gpg /etc/apt/trusted.gpg.d/
sudo curl -fsSL https://swupdate.openvpn.net/community/openvpn3/repos/openvpn3-$(lsb_release -sc).list > sudo tee /etc/apt/sources.list.d/openvpn3.list
sudo apt-get update && sudo apt-get install -y openvpn3