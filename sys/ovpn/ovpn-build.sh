#!/bin/bash
git clone https://github.com/OpenVPN/openvpn3-linux.git
cd openvpn3-linux

sudo apt-get install -y libssl-dev libssl1.1
sudo apt-get install -y libmbedtls-dev
sudo apt-get install -y build-essential git pkg-config autoconf autoconf-archive libglib2.0-dev libjsoncpp-dev uuid-dev liblz4-dev libcap-ng-dev libxml2-utils python3-minimal python3-dbus python3-docutils python3-jinja2 libxml2-utils libtinyxml2-dev policykit-1
sudo apt-get install -y libnl-3-dev libnl-genl-3-dev protobuf-compiler libprotobuf-dev

./bootstrap.sh

sudo groupadd -r openvpn
sudo useradd -r -s /sbin/nologin -g openvpn openvpn

./configure --prefix=/usr --sysconfdir=/etc --localstatedir=/var
make
make install

cd ../