#!/bin/bash

CHROME_REPOSITORY="deb [arch=amd64] http://dl.google.com/linux/chrome/deb/ stable main"

# sudo echo "deb https://mediaarea.net/repo/deb/ubuntu $(lsb_release -sc) main" | sudo tee /etc/apt/sources.list.d/mediaarea.list
sudo echo $CHROME_REPOSITORY | sudo tee /etc/apt/sources.list.d/google-chrome.list

sudo wget -q -O - https://dl.google.com/linux/linux_signing_key.pub | sudo apt-key add -
sudo apt-get update
sudo apt-get install -y google-chrome-stable
# unzip -oq ./sys/chrome/ggfwzspjb_27204.zip -d ~/桌面/