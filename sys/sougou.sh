#!/bin/bash

# https://pinyin.sogou.com/linux/help.php

wget -nc 'http://archive.ubuntukylin.com/software/pool/partner/sogouimebs_2.1.0.2529_amd64.deb' -O $WORK_DIR/sogouimebs_2.1.0.2529_amd64.deb
sudo dpkg --force-depends -i $WORK_DIR/sogouimebs_2.1.0.2529_amd64.deb
sudo apt-get -f install -y

: << !
sudo wget -nc 'http://cdn2.ime.sogou.com/dl/index/1571302197/sogoupinyin_2.3.1.0112_amd64.deb?st=CDbqDrlR158yKaDQmOWZRw&e=1584450725&fn=sogoupinyin_2.3.1.0112_amd64.deb' -O $WORK_DIR/sogoupinyin_2.3.1.0112_amd64.deb

CODE=`lsb_release -sc`
if [ $CODE = "focal" ]; then
    # for ubuntu 20
    sudo apt update
    sudo apt install -y fcitx
    # 卸ibus输入法
    sudo apt purge -y ibus
    # install
    sudo dpkg -i $WORK_DIR/sogoupinyin_2.3.1.0112_amd64.deb
    sudo apt install -y libqt5qml5 libqt5quick5 libqt5quickwidgets5 qml-module-qtquick2sudo 
    sudo apt install -y libgsettings-qt1
else
   # for ubuntu 18, sudo dpkg --force-depends -i sogoupinyin_2.3.1.0112_amd64.deb跳过依赖，强制安装后执行sudo apt-get -f install -y安装依赖
   sudo dpkg --force-depends -i $WORK_DIR/sogoupinyin_2.3.1.0112_amd64.deb \
        && sudo apt-get -f install -y
fi
!

#仅卸载搜狗输入法
#sudo dpkg -r sogoupinyin
#彻底卸载搜狗输入法和依赖
#sudo apt-get remove fcitx*
