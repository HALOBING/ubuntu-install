#!/bin/sh

## 为了兼容 bash与dash
# 1. 方法定义去除 function 关键字
# 2. 屏幕输出颜色 使用 printf "$COLOR str\n" 代替 echo -e "$COLOR str"
# 3. if [[]] 改为 if []
# 4. source /etc/profile 改为 . /etc/profile
##

# Colours Variables
RESTORE='\033[0m'
RED='\033[00;31m'
GREEN='\033[00;32m'
YELLOW='\033[00;33m'
BLUE='\033[00;34m'
PURPLE='\033[00;35m'
CYAN='\033[00;36m'
LIGHTGRAY='\033[00;37m'
LRED='\033[01;31m'
LGREEN='\033[01;32m'
LYELLOW='\033[01;33m'
LBLUE='\033[01;34m'
LPURPLE='\033[01;35m'
LCYAN='\033[01;36m'
WHITE='\033[01;37m'

# sudo 模式下获取当前登录用户名
USER_NAME=$(getent passwd `who` | head -n 1 | cut -d : -f 1)
WORK_DIR="/home/$USER_NAME"

export WORK_DIR=$WORK_DIR
export USER_NAME=$USER_NAME
export USER_DESKTOP=$WORK_DIR/Desktop

# 注意注意， 在当前sh中 打印 $PATH -> /usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/snap/bin, 需要调用 /etc/profile.d/*.sh 将java maven等path加上
. /etc/profile

which xdg-user-dir >/dev/null 2>&1
if [ "$?" -eq "0" ]; then
  # export USER_DESKTOP=$(xdg-user-dir DESKTOP)
  export USER_DESKTOP=$USER_DESKTOP
else
  # ln -s $WORK_DIR/Desktop $WORK_DIR/桌面
  export USER_DESKTOP=$WORK_DIR/桌面
fi

get_input_char() {
  SAVEDSTTY=$(stty -g)
  stty -echo
  stty cbreak
  dd if=/dev/tty bs=1 count=1 2>/dev/null
  stty -raw
  stty echo
  stty $SAVEDSTTY
}

pause() {
  if [ -n "$1" ]; then
    printf "$1\n"
  fi

  printf "$RED Press any key to continue!\n"
  char=$(get_input_char)
}

# Check Internet Conection
check_internet() {
  printf "$BLUE [ * ] Checking for internet connection\n"
  sleep 1
  echo -e "GET https://baidu.com HTTP/1.0\n\n" | nc baidu.com 80 >/dev/null 2>&1
  if [ $? -ne 0 ]; then
    printf "$RED [ X ]$BLUE Internet Connection ➜$RED OFFLINE!\n\n"
    printf "$RED Sorry, you really need an internet connection...."
    exit 0
  else
    printf "$GREEN [ ✔ ]$BLUE Internet Connection ➜$GREEN CONNECTED!\n\n"
    sleep 1
  fi
}

check_binary() {
  printf "$BLUE [ * ] Checking for $1\n"
  sleep 1
  bin=$1
  if [ $# -ge 2 ]; then
    bin=$2
  fi
  which $bin >/dev/null 2>&1
  if [ "$?" -eq "0" ]; then
    printf "$GREEN [ ✔ ]$BLUE $1 ➜$GREEN INSTALLED\n\n"
    sleep 1
    return 1
  else
    printf "$RED [ X ]$BLUE $1 ➜$RED NOT INSTALLED\n\n"
    return 0
  fi
}

check_pkg() {
  pkg=$1
  if [ $# -ge 2 ]; then
    pkg=$2
  fi
  check_binary $1 && sudo apt-get install -y $pkg
}

# 统一windows和linux双系统的时间
sync_sys_time() {
  # 检查 EFI 分区是否存在
  if [ ! -d "/sys/firmware/efi" ]; then
    return
  fi

  # 查找 EFI 分区设备路径
  efi_partition=$(df /boot/efi | grep -oP '\/dev\/\K.*(?= )')
  # 检查 EFI 分区是否有 Windows 引导文件
  if [ ! -e "$efi_partition/EFI/Microsoft/Boot/bootmgfw.efi" ]; then
    return
  fi

  set_local_rtc=$(timedatectl show --property=LocalRTC --value)
  if [ "$set_local_rtc" = "yes" ]; then
    return
  fi

  printf "$RESTORE 统一Windows和Ubuntu双系统的时间? (y)es, (n)o :\n"
  read -p ' ' INPUT
  case $INPUT in
  [Yy]*) sudo timedatectl set-local-rtc 1 --adjust-system-clock ;;
  [Nn]*) ;;
  *)
    printf "$RED\n Sorry, try again.\n"
    sync_sys_time
    ;;
  esac
}

enable_bash() {
  str="$(ls /bin/sh -al)"
  if echo "$str" | grep -q "bash"; then
    return
  fi
  # 24.04
  ln -sf /bin/bash /bin/sh
  return 
  printf "$RESTORE 启用bash? (y)es, (n)o :"
  read -p ' ' INPUT
  case $INPUT in
  [Yy]*)
    pause "$RED 在接下来弹窗中选择 “否” 启用bash！！！"
    # bash #https://blog.csdn.net/buynow123/article/details/51774018
    # 窗口中选择否
    sudo dpkg-reconfigure dash
    enable_bash
    ;;
  [Nn]*) enable_bash ;;
  *)
    printf "$RED\n Sorry, try again.\n"
    enable_bash
    ;;
  esac
}

change_mirror() {
  if [ -f "/etc/apt/sources.list_bak" ]; then
    return
  fi

  printf "$RED [ X ]$BLUE Setting up mirror ➜$RED tsinghua!\n\n"
  sleep 1

  cp /etc/apt/sources.list /etc/apt/sources.list_bak
  sed -i "s/us.archive.ubuntu.com/mirrors.tuna.tsinghua.edu.cn/g; s/cn.archive.ubuntu.com/mirrors.tuna.tsinghua.edu.cn/g; s/archive.ubuntu.com/mirrors.tuna.tsinghua.edu.cn/g" /etc/apt/sources.list
  sed -i -E "s@http://.*security.ubuntu.com@https://mirrors.tuna.tsinghua.edu.cn@g" /etc/apt/sources.list
  # 24.04
  cp /etc/apt/sources.list.d/ubuntu.sources /etc/apt/sources.list.d/ubuntu.sources_bak
  sed -i -E "s@URIs: .*(security|archive).ubuntu.com/ubuntu/@URIs: https://mirrors.tuna.tsinghua.edu.cn/ubuntu/@g" /etc/apt/sources.list.d/ubuntu.sources

  # 删除注释行
  # sed -i '/^#/d' /etc/apt/sources.list

  # lsb_release -sc 可获取系统代号

  apt-get -y update
  # apt-get -y upgrade
}

change_lang(){
  lang="$(echo $LANG | cut -d '.' -f 1)"
  if [ "$lang" != "zh_CN" ]; then
    printf "$RED [ X ]$BLUE Setting up lang ➜$RED zh_CN!\n\n"
    sleep 1
    apt-get install -y language-pack-zh-hans language-pack-zh-hans-base
    timedatectl set-timezone Asia/Shanghai
    update-locale LANG="zh_CN.UTF-8" LANGUAGE="zh_CN:zh" LC_ALL="zh_CN.UTF-8"
    #sed -i 's/en_US/zh_CN/g' /etc/default/locale
    #update-locale
    locale-gen zh_CN.UTF-8
  fi
}

create_pip_conf() {
  if [ -d "$WORK_DIR/.pip" ]; then
    return
  fi
  # pip
  mkdir -p $WORK_DIR/.pip
  cat > $WORK_DIR/.pip/pip.conf <<EOF
[global]
index-url = https://pypi.tuna.tsinghua.edu.cn/simple
[install]
trusted-host = https://pypi.tuna.tsinghua.edu.cn
EOF
}

cekfont(){
    printf "$BLUE [ * ] Checking for Segoe-UI Font\n"
    sleep 1
    fc-list | grep -i "Segoe UI" >/dev/null 2>&1
    if [ "$?" -eq "0" ]; then
        printf "$GREEN [ ✔ ]$BLUE Segoe-UI Font ➜$GREEN INSTALLED\n\n"
        sleep 1
    else
        printf "$RED [ X ]$BLUE Segoe-UI Font ➜$RED NOT INSTALLED\n\n"
        continueFont
    fi
}

continueFont(){
    printf "$RESTORE Do you want to install Segoe-UI Font? (y)es, (n)o :\n"
    read  -p ' ' INPUT
    case $INPUT in
    [Yy]* ) fontinstall;;
    [Nn]* ) end;;
    * ) printf "$RED\n Sorry, try again.\n"; continueFont;;
  esac
}

enable_safe_rm(){
    #safe-rm="$(readlink -f $(which rm))"
    if [ -n "$(ls -l /usr/local/bin/rm | grep safe-rm)" ]; then
        return
    fi
    printf "$RED [ X ]$BLUE Setting up rm ➜$RED safe-rm!\n\n"
    sleep 1
    sudo apt-get install -y safe-rm
    sudo ln -s /usr/bin/safe-rm /usr/local/bin/rm
}

sys_setup() {

  check_internet
  enable_bash
  sync_sys_time
  change_mirror
  change_lang
  create_pip_conf
  enable_safe_rm

  check_pkg curl
  check_pkg unzip
  check_pkg rar
  check_pkg unrar
  check_pkg 7z p7zip-full
  check_pkg vim
  check_pkg git
  check_pkg baobab # 磁盘分析工具
  check_pkg sshd openssh-server
  check_pkg ifconfig net-tools
  check_pkg mount.exfat-fuse exfat-utils # 无法挂载exfat格式U盘
  check_pkg gcc build-essential
  check_pkg python3
  check_pkg pip3 python3-pip
  check_pkg mysqldump mysql-client-core-8.0
  check_pkg cmake cmake
  check_pkg proxychains4 proxychains4 # sudo vim /etc/proxychains4.conf

  # sudo chown -R ${USER_NAME}:${USER_NAME} /usr/local
}

install_sys() {
   sys_setup

  # dpkg -l | grep baidu* # 模糊检索已安装的包
  # dpkg -L fcitx-baidupinyin:amd64 # 查看包安装位置

  check_binary "Microsoft Edge" microsoft-edge-stable && sh ./sys/edge.sh
  check_binary "Google Chrome" google-chrome-stable && sh ./sys/chrome.sh
  check_binary "Docker Engine" docker && sh ./sys/docker.sh
  check_binary "Docker Compose" docker-compose && sh ./sys/docker-compose.sh
  # check_binary "Baidu PinYin" /opt/apps/com.baidu.fcitx-baidupinyin/files/bin/baidu-qimpanel && sh ./sys/baidupinyin/install.sh
  check_binary "Fcitx5" fcitx5 && sh ./sys/fcitx5/install.sh
  check_binary "WPS 2019" wps && sh ./sys/wps/install.sh
  check_binary "Winxin Linux" weixin && sh ./sys/weixin.sh
  check_binary "Xunlei" /opt/apps/com.xunlei.download/files/thunder && sh ./sys/xunlei/install.sh
  check_binary "Remmina" remmina && sh ./sys/remmina.sh
  check_binary "Flameshot" flameshot && sh ./sys/flameshot.sh
  check_binary "Openvpn3" openvpn3 && sh ./sys/ovpn/install.sh
  check_binary "Anaconda" $WORK_DIR/anaconda3/bin/conda && sh ./sys/anaconda.sh
}

install_dev() {
  check_binary "Java" java && sh ./dev/java/install.sh
  check_binary "Maven" mvn && sh ./dev/maven/install.sh
  check_binary "Nodejs" node && sh ./dev/nodejs.sh
  check_binary "Golang" go && sh ./dev/go.sh
  check_binary "Intellij IDEA" $USER_DESKTOP/jetbrains-idea.desktop && sh ./dev/idea/install.sh
  check_binary "DBeaver" /usr/share/dbeaver-ce/dbeaver && sh ./dev/dbeaver.sh

  echo "done."
}


printf_sys_info() {

  printf "$BLUE Checking for Docker Engine ➜$RESTORE $(docker -v)\n"

  printf "$BLUE Checking for Docker Compose ➜$RESTORE $(docker-compose -v)\n"

  printf "$BLUE Checking for Python3 ➜$RESTORE $(python3 -V)\n"

}

printf_dev_info() {

  . /etc/profile

  printf "$BLUE Checking for DBeaver local client ➜$RESTORE $(whereis -b mysqldump)\n"

  printf "$BLUE Checking for Nodejs ➜$RESTORE $(node -v)\n"

  printf "$BLUE Checking for Golang ➜$RESTORE $(go version)\n"

  printf "$BLUE Checking for Java ➜$RESTORE\n" && java -version

  printf "$BLUE Checking for Maven ➜$RESTORE\n" && mvn -v

  printf "$BLUE IDEA配置说明 ➜$RESTORE\n" && cat ./dev/idea/readme.md

  printf "$BLUE 百度输入法配置 ➜$RESTORE\n"
  echo "1. 打开 \"语言支持\""
  echo "2. 依次点击 \"确定\" -> \"yes\" -> 选中 \"fcitx   启用小企鹅输入法Fcitx"
  echo "3. 选择 \"附加组件\" -> 简繁切换 点击底部 \"配置\" -> 点击ctrl+shift+F，按Esc键，删除该快捷键"
  echo "4. 最后reboot"

  echo ""
}

###########################
# main
###########################

if [ $(id -u) -ne 0 ] ; then
  echo "This script requires sudo privileges."
  exit 1
fi

case "$1" in
'sys')
  install_sys
  ;;
'dev')
  install_dev
  ;;
'printf')
  printf_sys_info
  printf_dev_info
  ;;
'all')
  install_sys
  install_dev
  ;;
*)
  echo "Usage: $0 {sys|dev|all|printf <command>}" >&2
  exit 1
  ;;

esac

exit 0
