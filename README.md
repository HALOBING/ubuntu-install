# 安装完 ubuntu 18.04/20.04 后一键安装 系统常用工具以及java、js开发环境

## 安装chrome、edge、wps、weixin、baidupinyin、xunlei、remmina、docker、idea、java、maven、golang、redis、unzip、unrar、vim、nodejs、python3、anaconda3、gcc等
```shell
sudo apt-get install -y git \
 && git config --global http.postBuffer 524288000 \
 && git config --global http.sslVerify false

git clone https://gitee.com/HALOBING/ubuntu-install.git ~/ubuntu-install \
 && cd ~/ubuntu-install/ \
 && sudo sh install.sh all
```

## 查看安装结果
```shell
sudo sh install.sh printf
```


## 文档
- <a href="docs/Ubuntu root账户的配置.md">Ubuntu root账户配置</a>
- <a href="docs/Ubuntu USB光驱的使用.md">Ubuntu USB光驱的使用</a> 

## 提取deb文件
- 提取系统已安装
```shell
sudo dpkg --get-selections > packages.txt
sudo apt-get download $(awk '{print $1}' < packages.txt)

```
- 提取指定包
```
sudo apt install -y apt-rdepends
apt download $(apt-rdepends vim | grep -v "^ ")

# fix E: Can't select candidate version from package debconf-2.0 as it has no candidate
apt-get download $(apt-rdepends vim | grep -v "^ " | sed 's/debconf-2.0/debconf/g')
```