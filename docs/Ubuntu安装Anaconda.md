安装Anaconda
---
```shell
apt install libgl1-mesa-glx libegl1-mesa libxrandr2 libxrandr2 libxss1 libxcursor1 libxcomposite1 libasound2 libxi6 libxtst6

wget https://repo.anaconda.com/archive/Anaconda3-2022.10-Linux-x86_64.sh
bash Anaconda3-2022.10-Linux-x86_64.sh

conda config --add channels https://mirrors.tuna.tsinghua.edu.cn/anaconda/pkgs/free/
conda config --add channels https://mirrors.tuna.tsinghua.edu.cn/anaconda/pkgs/main/
conda config --set show_channel_urls yes
# 关闭自动启动base命令
conda config --set auto_activate_base false
```

激活系统Python环境
```shell
conda init bash
conda list

conda search "^python$"
conda create --name my_env python=3
conda activate my_env
python --version

# 当您准备停用Anaconda环境时,可以通过键入以下内容来执行此操作
conda deactivate

```

创建python虚拟环境
```shell
# 要针对更具体的Python版本,可以将特定版本传递给python参数,例如3.5,例如
conda create -n my_env35 python=3.5
# 您可以检查使用此命令设置的所有环境
conda info --envs
# 删除虚拟环境
conda remove --name my_env35 --all
```

迁移Conda缓存目录
```shell 
vim $HOME/.condarc

envs_dirs:
  - /work/home/fuyb/Anaconda3/envs
  - /work/home/fuyb/.conda/envs
  - /work/home/fuyb/conda/conda/envs
pkgs_dirs:
  - /work/home/fuyb/Anaconda3/pkgs
  - /work/home/fuyb/.conda/pkgs
  - /work/home/fuyb/conda/conda/pkgs

conda info #查看是否生效

```

删除缓存
```shell
# 清空缓存
conda clean -p
# 删除无用包
conda clean -y --all
```
