训练环境
---

安装要求：  
cuda 的driver API 和 runtime API:  
driver cuda version >= toolkit cuda version  
cuda toolkit cuda version == paddlepaddle-gpu  

1. 安装 paddlepaddle 环境

- Linux docker（推荐）  
先安装 <a href="Ubuntu安装nvidia-docker.md">nvidia-docker2</a>
```shell
sudo nvidia-docker pull registry.baidubce.com/paddlepaddle/paddle:2.4.2-gpu-cuda10.2-cudnn7.6-trt7.0
sudo nvidia-docker run --name paddle_docker -it --shm-size="4g" -v /home/paddle/gpu/2.4.2:/paddle -v /home/develop:/home/develop registry.baidubce.com/paddlepaddle/paddle:2.4.2-gpu-cuda10.2-cudnn7.6-trt7.0 /bin/sh
```

- Anaconda环境
	- <a href="Ubuntu安装Anaconda.md">安装Anaconda</a>
	- <a href="Ubuntu安装CUDA&Cudnn&TensorRT.md">安装CUDA&Cudnn&TensorRT<a/>
	- 安装paddlepaddle

	```shell
	# 创建虚拟环境
	conda create -n paddle python=3.8
	# 激活虚拟环境
	conda activate paddle
	# pip 升级
	pip install --upgrade pip

	# 推荐使用conda 安装 paddlepaddle， 可以直接看到cuDNN版本
	conda install paddlepaddle-gpu==2.4.2 cudatoolkit=11.2 -c https://mirrors.tuna.tsinghua.edu.cn/anaconda/cloud/Paddle/ -c conda-forge
	# pip 安装不行不行的
	# python -m pip install paddlepaddle-gpu==2.3.2.post112 -f https://www.paddlepaddle.org.cn/whl/linux/mkl/avx/stable.html

	```

验证安装是否成功（EQ: PaddlePaddle is installed successfully!）
```shell
python
import paddle 
paddle.utils.run_check()
# GPU 是否可用
paddle.fluid.is_compiled_with_cuda()
```

如果一直无法识别gpu：
- 可以确认一下paddle版本和CUDA版本是否匹配
- 可能装有cpu版本的paddlepaddle，卸载即可
```shell
pip list | grep paddle
```
- 另外之前有遇到过重启能够解决这个问题

训练模型
---

1. 获取PaddleOCR
```shell
git clone -b release/2.6 https://gitee.com/paddlepaddle/PaddleOCR.git
cd PaddleOCR
# 安装第三方库 paddleocr 路径下
pip install -r requirements.txt -i https://pypi.tuna.tsinghua.edu.cn/simple
pip install protobuf==3.20.0 pyyaml -i https://pypi.tuna.tsinghua.edu.cn/simple
```

2. 模型推理
使用如下命令即可快速进行模型推理
```python
import os
import cv2
from paddleocr import PPStructure

table_engine = PPStructure(layout=False, show_log=False)

save_folder = './output'
img_path = 'table.jpg'
img = cv2.imread(img_path)
result = table_engine(img)

html_str = result[0]['res']['html']
# 显示标注的html字符串
from IPython.core.display import display, HTML
display(HTML(html_str))
```
```text
<IPython.core.display.HTML object>
```

3. 数据准备
https://aistudio.baidu.com/aistudio/datasetdetail/165849

本例中使用的数据集采用表格<a href="https://github.com/WenmuZhou/TableGeneration">生成工具</a>制作而成。

使用如下命令对数据集进行解压，并查看数据集大小
```text
mkdir -p ~/data/data165849 && cd /home/develop/data/data165849 && tar -xf table_gen_dataset.tar && cd -
wc -l /home/develop/data/data165849/table_gen_dataset/gt.txt
```
```text
tar: Ignoring unknown extended header keyword 'LIBARCHIVE.xattr.com.apple.macl'
tar: Ignoring unknown extended header keyword 'LIBARCHIVE.xattr.com.apple.lastuseddate#PS'
/home/aistudio
20000 /home/develop/data/data165849/table_gen_dataset/gt.txt
```

解压完最好检查一下有没有破损图片
```python
from glob import glob;

import numpy as np
from PIL import Image;

for p in glob("/home/develop/data/data165849/table_gen_dataset/img/*.png"):
    print(p)
    im = Image.open(p)
    im = np.array(im)
    im[im==255] = 0
    assert im.max() < 2 and im.min() >= 0
```

数据集解压完成后，使用下述命令将数据集划分为训练集和测试集, 这里将90%划分为训练集，10%划分为测试集
```python
import random
with open('/home/develop/data/data165849/table_gen_dataset/gt.txt') as f:
    lines = f.readlines()
random.shuffle(lines)
train_len = int(len(lines)*0.9)
train_list = lines[:train_len]
val_list = lines[train_len:]

# 保存结果
with open('/home/develop/train.txt','w',encoding='utf-8') as f:
    f.writelines(train_list)
with open('/home/develop/val.txt','w',encoding='utf-8') as f:
    f.writelines(val_list)
```

划分完成后，数据集信息如下
```text
类型	数量	图片地址	标注文件路径
训练集	18000	/home/develop/data/data165849/table_gen_dataset	/home/develop/train.txt
测试集	2000	/home/develop/data/data165849/table_gen_dataset	/home/develop/val.txt
```

4. 针对表格识别模型的优化
这里选用PP-Structurev2中的表格识别模型SLANet。

SLANet是PP-Structurev2中全新推出的表格识别模型，相比PP-Structurev1中TableRec-RARE，在预测速度不变的情况下精度提升4.7%。TEDS提升2%。

```text
算法	Acc	TEDS(Tree-Edit-Distance-based Similarity)	Speed
EDD[2]	x	88.3%	x
TableRec-RARE(ours)	71.73%	93.88%	779ms
SLANet(ours)	76.31%	95.89%	766ms
```

本例中，主要使用SLANet进行finetune训练来演示如何在自己的场景下进行表格识别模型的训练。

5. 模型训练和评估
进行训练之前先使用如下命令下载预训练模型
```shell
# 进入PaddleOCR工作目录
cd PaddleOCR
# 下载中文预训练模型
wget  -nc -P  ./pretrain_models/  https://paddleocr.bj.bcebos.com/ppstructure/models/slanet/ch_ppstructure_mobile_v2.0_SLANet_train.tar --no-check-certificate
cd ./pretrain_models/ && tar xf ch_ppstructure_mobile_v2.0_SLANet_train.tar  && cd ../
```

开始训练之前，可以先通过如下命令查看预训练模型在数据集上的精度
```shell
python tools/eval.py -c configs/table/SLANet_ch.yml -o \
        Global.pretrained_model=./pretrain_models/ch_ppstructure_mobile_v2.0_SLANet_train/best_accuracy.pdparams \
        Eval.dataset.data_dir=/home/develop/data/data165849 \
        Eval.dataset.label_file_list=[/home/develop/val.txt] \
	Eval.loader.batch_size_per_card=6
```

执行完成后可看见如下输出：

```text
[2022/11/24 11:08:13] ppocr INFO: metric eval ***************
[2022/11/24 11:08:13] ppocr INFO: acc:0.12649999993674998
[2022/11/24 11:08:13] ppocr INFO: fps:31.883146778372737
```

可以看到，直接使用预训练模型进行评估，在数据集生准确率只有12.65%，我们接下来可以通过finetune的方式来提升模型在数据集上的准确率。

使用如下命令即可启动训练，需要修改的配置及其含义如下：

```text
字段	修改值	含义
Global.pretrained_model	./pretrain_models/ch_ppstructure_mobile_v2.0_SLANet_train/best_accuracy.pdparams	指向英文表格预训练模型地址
Global.eval_batch_step	375	模型多少step评估一次，一般设置为一个epoch总的step数
Optimizer.lr.name	Const	学习率衰减器
Optimizer.lr.learning_rate	0.0005	学习率设为之前的0.05倍
Optimizer.lr.warmup_epoch	0	学习率预热设为0
Train.dataset.data_dir	/home/develop/data/data165849	指向训练集图片存放目录
Train.dataset.label_file_list	/home/develop/train.txt	指向训练集标注文件
Eval.dataset.data_dir	/home/develop/data/data165849	指向测试集图片存放目录
Eval.dataset.label_file_list	/home/develop/val.txt	指向测试集标注文件
```

开始训练

- use gpu:
```shell
python tools/train.py -c configs/table/SLANet_ch.yml -o \
    Global.pretrained_model=./pretrain_models/ch_ppstructure_mobile_v2.0_SLANet_train/best_accuracy.pdparams \
    Global.save_model_dir=output/SLANet_ch/ \
    Global.epoch_num=7 \
    Global.save_epoch_step=1 \
    Global.eval_batch_step="[1, 375]" \
    Global.print_batch_step=5 \
    Optimizer.lr.name=Const \
    Optimizer.lr.learning_rate=0.0005 \
    Optimizer.lr.warmup_epoch=0 \
    Train.dataset.data_dir=/home/develop/data/data165849 \
    Train.dataset.label_file_list=[/home/develop/train.txt] \
    Train.loader.batch_size_per_card=3 \
    Train.loader.num_workers=1 \
    Eval.dataset.data_dir=/home/develop/data/data165849 \
    Eval.dataset.label_file_list=[/home/develop/val.txt] \
    Eval.loader.batch_size_per_card=3 \
    Eval.loader.num_workers=1
```

- use cpu:
```shell
conda install paddlepaddle==2.4.2 --channel https://mirrors.tuna.tsinghua.edu.cn/anaconda/cloud/Paddle/
python tools/train.py -c configs/table/SLANet_ch.yml -o \
    Global.use_gpu=False \
    Global.use_sync_bn=False \
    Global.epoch_num=7 \
    Global.save_epoch_step=1 \
    Global.pretrained_model=./pretrain_models/ch_ppstructure_mobile_v2.0_SLANet_train/best_accuracy.pdparams \
    Global.save_model_dir=output/SLANet_ch/ \
    Global.eval_batch_step="[1, 375]" \
    Global.print_batch_step=5 \
    Optimizer.lr.name=Const \
    Optimizer.lr.learning_rate=0.0005 \
    Optimizer.lr.warmup_epoch=0 \
    Train.dataset.data_dir=/home/develop/data/data165849 \
    Train.dataset.label_file_list=[/home/develop/train.txt] \
    Train.loader.batch_size_per_card=4 \
    Eval.dataset.data_dir=/home/develop/data/data165849 \
    Eval.dataset.label_file_list=[/home/develop/val.txt] \
    Eval.loader.batch_size_per_card=4
```

训练过程中可看见如下输出：
```text
[2022/11/23 14:49:15] ppocr INFO: load pretrain successful from ./pretrain_models/ch_ppstructure_mobile_v2.0_SLANet_train/best_accuracy
[2022/11/23 14:50:15] ppocr INFO: epoch: [1/7], global_step: 20, lr: 0.000500, acc: 0.000000, loss: 0.249648, structure_loss: 0.033885, loc_loss: 0.216119, avg_reader_cost: 0.03019 s, avg_batch_cost: 3.02216 s, avg_samples: 32.0, ips: 10.58847 samples/s, eta: 3:17:08
```

finetune最高精度为 98.74%


训练完成后，可使用如下命令在测试集上评估最优模型的精度
```shell
python tools/eval.py -c configs/table/SLANet_ch.yml -o \
    Global.checkpoints=output/SLANet_ch/best_accuracy.pdparams \
    Eval.dataset.data_dir=/home/develop/data/data165849 \
    Eval.dataset.label_file_list=[/home/develop/val.txt] \
    Eval.loader.batch_size_per_card=10
```

执行完成后可看见如下输出：
```text
[2022/11/24 09:46:57] ppocr INFO: metric eval ***************
[2022/11/24 09:46:57] ppocr INFO: acc:0.9874999995062499
[2022/11/24 09:46:57] ppocr INFO: fps:29.85860919474217
```

6. 模型推理
在实际部署中，我们需要将动态图训练的模型转换为静态图模型进行推理，使用如下命令可以将训练好的模型导出为静态图模型:
```shell
python tools/export_model.py -c configs/table/SLANet_ch.yml -o \
            Global.checkpoints=output/SLANet_ch/best_accuracy.pdparams \
            Global.save_inference_dir=output/SLANet_ch/infer
```

执行完成后可看见如下输出：
```text
W0728 03:06:27.772702  2260 gpu_resources.cc:61] Please NOTE: device: 0, GPU Compute Capability: 3.5, Driver API Version: 11.4, Runtime API Version: 10.2
W0728 03:06:27.775954  2260 gpu_resources.cc:91] device: 0, cuDNN Version: 7.6.
https://paddle-imagenet-models-name.bj.bcebos.com/dygraph/legendary_models/PPLCNet_x1_0_ssld_pretrained.pdparams
[2023/07/28 03:06:28] ppocr INFO: resume from output/SLANet_ch/best_accuracy
[2023/07/28 03:06:32] ppocr INFO: inference model is saved to output/SLANet_ch/infer/inference
```

导出后的模型，使用如下命令可使用预测引擎对单张图片进行推理：
```shell
cd ppstructure
python table/predict_structure.py \
    --table_model_dir=../output/SLANet_ch/infer \
    --table_char_dict_path=../ppocr/utils/dict/table_structure_dict_ch.txt \
    --image_dir=../train/table.jpg \
    --output=../output/inference
```

执行完成后可看见如下输出：
```text
[2023/07/28 03:11:38] ppocr INFO: save vis result to ../output/inference/table.jpg
[2023/07/28 03:11:38] ppocr INFO: Predict time of ../train/table.jpg: 1.0698575973510742
```

也可通过如下命令可视化单元格坐标:
```python
import cv2
from matplotlib import pyplot as plt

# 显示预测的单元格
show_img = cv2.imread('/home/develop/projects/PaddleOCR/output/inference/table.jpg')
plt.figure(figsize=(15,15))
plt.imshow(show_img)
plt.show()
```

在表格结构模型训练完成后，可结合OCR检测识别模型，对表格内容进行识别。

首先下载PP-OCRv3文字检测识别模型
```shell
wget  -nc -P  ./inference/ https://paddleocr.bj.bcebos.com/PP-OCRv3/chinese/ch_PP-OCRv3_det_infer.tar --no-check-certificate
wget  -nc -P  ./inference/ https://paddleocr.bj.bcebos.com/PP-OCRv3/chinese/ch_PP-OCRv3_rec_infer.tar --no-check-certificate
cd ./inference/ && tar xf ch_PP-OCRv3_det_infer.tar && tar xf ch_PP-OCRv3_rec_infer.tar  && cd ../
```

模型下载完成后，使用如下命令进行表格识别
```shell
 python table/predict_table.py \
    --det_model_dir=inference/ch_PP-OCRv3_det_infer \
    --rec_model_dir=inference/ch_PP-OCRv3_rec_infer  \
    --table_model_dir=../output/SLANet_ch/infer \
    --rec_char_dict_path=../ppocr/utils/ppocr_keys_v1.txt \
    --table_char_dict_path=../ppocr/utils/dict/table_structure_dict_ch.txt \
    --image_dir=../train/table.jpg \
    --output=../output/table
```

 python table/predict_table.py \
    --det_model_dir=inference/ch_PP-OCRv3_det_infer \
    --rec_model_dir=inference/ch_PP-OCRv3_rec_infer  \
    --table_model_dir=/home/develop/Downloads/submit_2_28_3/ppyoloe_plus_crn_x_80e_coco \
    --rec_char_dict_path=../ppocr/utils/ppocr_keys_v1.txt \
    --table_char_dict_path=../ppocr/utils/dict/table_structure_dict_ch.txt \
    --image_dir=../train/table.jpg \
    --output=../output/table

命令运行结束后，可检测如下输出：
```text
[2023/07/28 03:19:28] ppocr INFO: [0/1] ../train/table.jpg
[2023/07/28 03:19:29] ppocr DEBUG: dt_boxes num : 105, elapse : 0.27765560150146484
[2023/07/28 03:19:31] ppocr DEBUG: rec_res num  : 105, elapse : 1.7107703685760498
.
.
.
[2023/07/28 03:19:32] ppocr INFO: excel saved to ../output/table/table.xlsx
[2023/07/28 03:19:32] ppocr INFO: Predict time : 4.388s

```

也可通过如下命令进行html的可视化:
```python
show_img = cv2.imread('../train/table.jpg')
plt.figure(figsize=(15,15))
plt.imshow(show_img)
plt.show()

# 显示预测结果
from IPython.core.display import display, HTML
display(HTML('<html><body><table><tr><td rowspan="2">投资，营造更好投资环</td><td colspan="3">专栏，3月1</td></tr><tr><td>，解决停车</td><td>组听取意见，与</td><td>工作人员的</td></tr><tr><td>uctlity</td><td></td><td></td><td>1.23</td></tr><tr><td>Manag</td><td>47</td><td>s7593</td><td>84.63</td></tr></table></body></html>'))
```


问题
---

1. ValueError: Target 0 is out of lower bound
- conda 安装的cudatoolkit、cudnn 版本与系统的不一致导致的环境问题
- 使用 2.3.2 遇到此问题，换 2.4.2 就行
- 安装完环境重启电脑一下试试

2. 使用cpu eval时可以看到acc大于0, gpu一直为0 且训练时候 acc 0.000000, loss nanxxx
- paddlepaddle cuda选择低版本试试，之前使用11.2一直不成功，换10.2后成功

3. Please use command `df -h` to check the storage space of `/dev/shm`. Shared storage space needs to be greater than
- 在dockers里创建容器时时候加 --shm-size="4g" 
- 不在docker里，修改 /etc/fstab
```text
# size为总内存的一半
tmpfs /dev/shm tmpfs defaults,size=4g 0 0
```

