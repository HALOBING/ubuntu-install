## 设置root密码
```
sudo passwd

# 验证密码
su root
```

## 启动root登录
```
sudo vim /usr/share/lightdm/lightdm.conf.d/50-ubuntu.conf

[Seat:*]
user-session=ubuntu
greeter-show-manual-login= true
all-guest=false #这个可以 不用配置


sudo vim /etc/pam.d/gdm-autologin
注释：auth required pam_succeed_if.so user != root quiet_success

sudo vim /etc/pam.d/gdm-password
注释：auth required pam_succeed_if.so user != root quiet_success

sudo vim /root/.profile

# ~/.profile: executed by Bourne-compatible login shells.

if [ "$BASH" ]; then
  if [ -f ~/.bashrc ]; then
    . ~/.bashrc
  fi
fi
tty -s && mesg n || true
mesg n || true


```

## 如果在root下安装idea，切换到普通用户后桌面没有快捷方式：
```
su root

cp /root/jetbrains-idea.desktop /home/fuyb/桌面
chown fuyb:fuyb /home/fuyb/桌面/jetbrains-idea.desktop

```