准备工作
--
1. 下载GCC-9.3.0安装包
```shell
wget https://mirrors.tuna.tsinghua.edu.cn/gnu/gcc/gcc-9.3.0/gcc-9.3.0.tar.gz
```
2. 解压安装包
```shell
tar -xvf gcc-9.3.0.tar.gz
```
3. 进入GCC解压目录
```shell
cd gcc-9.3.0
```
4. 下载依赖版本gmp-6.1.0
```shell
wget https://mirrors.tuna.tsinghua.edu.cn/gnu/gmp/gmp-6.1.0.tar.xz
tar -xvf gmp-6.1.0.tar.xz
mv gmp-6.1.0 gmp
```
5. 下载依赖版本mpfr-3.1.4
```shell
wget https://mirrors.tuna.tsinghua.edu.cn/gnu/mpfr/mpfr-3.1.4.tar.gz
tar -xvf mpfr-3.1.4.tar.gz
mv mpfr-3.1.4 mpfr
```
6. 下载依赖版本mpc-1.0.3
```shell
wget https://mirrors.tuna.tsinghua.edu.cn/gnu/mpc/mpc-1.0.3.tar.gz
tar -xvf mpc-1.0.3.tar.gz
mv mpc-1.0.3 mpc
```
其依赖关系为 gcc->mpc->mpfr->gmp->m4


编译安装
---
1. 设置配置信息
```shell
mkdir gcc-build
cd gcc-build

#（/usr/local/gcc-9.3.0是新版本GCC的安装目录，可以更换，原始gcc运行目录为/usr/bin/gcc）
../configure --prefix=/usr/local/gcc-9.3.0 --disable-multilib --enable-languages=c,c++

```
2. 编译
```shell
# 8核cpu，可根据cpu核数设置，因为编译过程很长，尽可能使用全部cpu资源
make -j 10  
```
3. 安装
```shell
make install -j 10
```
4. 替换gcc cc命令的链接
```shell
ln -s /usr/local/gcc-9.3.0 /usr/local/gcc
ln -s /usr/local/gcc-9.3.0/bin/gcc /usr/local/cc
ln -s /usr/local/gcc-9.3.0/bin/gcc /usr/bin/cc
# 如果发生错误 可以先备份原来的链接比如
mv /usr/local/gcc /usr/local/gcc.bak
mv /usr/local/cc /usr/local/cc.bak
mv /usr/bin/cc /usr/bin/cc.bak
```
5. 配置环境变量
```shell
# 将以下配置写入环境变量中，如全局变量/etc/profile 或个人变量~./.bashrc 文件最后

export PATH=/usr/local/gcc/bin:$PATH
export LD_LIBRARY_PATH=/usr/local/gcc/lib64
export MANPATH=/usr/local/gcc/share/man:$MANPATH

source ~./.bashrc
```
6. 检查安装是否成功
```shell
gcc -v
g++ -v
cc -v
```
```text
Using built-in specs.
COLLECT_GCC=gcc
COLLECT_LTO_WRAPPER=/usr/local/gcc-9.3.0/libexec/gcc/x86_64-pc-linux-gnu/9.3.0/lto-wrapper
Target: x86_64-pc-linux-gnu
Configured with: ../configure --prefix=/usr/local/gcc-9.3.0 --disable-multilib --enable-languages=c,c++
Thread model: posix
gcc version 9.3.0 (GCC) 
```

PS: https://www.jianshu.com/p/3931ca0d8275