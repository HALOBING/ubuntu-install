有线网卡
---

查看网卡型号
```shell
lspci | grep net
```
```text
02:00.0 Ethernet controller: Realtek Semiconductor Co., Ltd. RTL8111/8168/8411 PCI Express Gigabit Ethernet Controller (rev 0c)
```

可以看到我这里的型号是 8111/8168/8411 (任意一个都可以驱动)

查看网卡驱动 
```shell
lspci -k | grep modules
```
```text
	Kernel modules: xhci_pci
	Kernel modules: ahci
	Kernel modules: nvidiafb, nvidia_drm, nvidia
	Kernel modules: r8168
```

这里 找到了r8168说明驱动没问题，因为我装了，如果数字和型号不对应那么需要先卸载
```shell
# 找到网卡驱动的位置 并删除
modinfo r8168 | grep filename
sudo rm -rf <找到的path>/r8168.ko
​
# 卸载驱动
sudo rmmod r8168
​
# 查看是否卸载成功
sudo lsmod | grep r8168
```


然后是要下载对应的驱动，网址：https://www.realtek.com/en/component/zoo/category/network-interface-controllers-10-100-1000m-gigabit-ethernet-pci-express-software


这里选择下载 8.051.02 版本的 r8168 的网卡驱动，然后进行驱动安装。

```shell
# 解压到 /usr/src
sudo tar xvf r8168-8.051.02.tar.bz2  -C /usr/src
​
cd /usr/src/r8168-8.051.02
touch dkms.conf
```

在 dkms.conf 中输入如下内容：

```text
PACKAGE_NAME=Realtek_r8168
PACKAGE_VERSION=8.051.02
​
DEST_MODULE_LOCATION=/updates/dkms
BUILT_MODULE_NAME=r8168
BUILT_MODULE_LOCATION=src/
​
MAKE="'make' -C src/ all"
CLEAN="'make' -C src/ clean"
AUTOINSTALL="yes"
```
然后继续：

```shell
# 安装 dkms
sudo apt update
sudo apt install dkms
​
# 编译dkms 并 挂载驱动
sudo dkms add -m r8168 -v 8.051.02
sudo dkms build -m r8168 -v 8.051.02
sudo dkms install -m r8168 -v 8.051.02
sudo depmod -a
sudo modprobe r8168
```
这样就大功告成了，这个时候你回到桌面右上角看，就会有 有线网络图标 了。

另外再附一个疑难杂症：有时候我们做了其他一些操作，在重启后可能发现有线网络又不行了，有线网络的图标也不见了。这个时候我们自然会想着重新挂载一下吧，但又出现了 modprobe: ERROR: could not insert 'r8168': Exec format error 的报错，该怎么办呢？

这里也附上解决方法：

```shell
# 先卸载掉 r8168 驱动，根据自己的驱动版本号来
sudo dkms remove r8168/8.051.02 --all
​
# 然后 cd 到驱动包路径，执行如下操作
cd /usr/src/r8168-8.051.02
​
sudo dkms add -m r8168 -v 8.051.02 
sudo dkms build -m r8168 -v 8.051.02
sudo dkms install -m r8168 -v 8.051.02
sudo depmod -a
sudo modprobe r8168
```
这样就把前面的问题解决了。
