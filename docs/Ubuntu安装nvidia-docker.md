0. 前提
```text
(1) 安装docker  
(2) 安装Nvidia驱动，CUDA和CUDNN  
```

1. 添加源
```shell
curl -s -L https://nvidia.github.io/nvidia-docker/gpgkey | sudo apt-key add -
curl -s -L https://nvidia.github.io/nvidia-docker/ubuntu18.04/nvidia-docker.list | sudo tee /etc/apt/sources.list.d/nvidia-docker.list
sudo apt-get update
```
2. 安装nvidia-docker2软件包并重新加载docker守护程序配置
```shell
sudo apt-get install nvidia-docker2
sudo systemctl daemon-reload
sudo systemctl restart docker
```
3. 验证安装
```shell
sudo docker info | grep  Runtimes
```
```text
Runtimes: io.containerd.runc.v2 nvidia runc
```
如果没有Runtimes
```shell
sudo vim /etc/docker/daemon.json
```
```json
{
  "runtimes": {
    "nvidia": {
      "path": "/usr/bin/nvidia-container-runtime",
      "runtimeArgs": []
    }
  }
}
```
```shell
sudo systemctl daemon-reload
sudo systemctl restart docker
```

[注]：运行`nvidia-docker run`若报错：
```text
docker: Error response from daemon: unknown or invalid runtime name: nvidia.
See 'docker run --help'.
```
需要在`daemon.json`里添加`runtimes`配置
