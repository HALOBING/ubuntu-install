1. 查看当前系统内核
```shell
uname -r
```
```text
(base) develop@develop-QiTianM415-D337:~$ uname -r
5.8.0-63-generic
```

2. 查询已安装的内核
```shell
dpkg -l | grep linux-image
```

4. 如果切换的内核不存在，检索需要安装的包并安装
```shell
sudo apt search linux-image-5.8
sudo apt-get install linux-image-5.8.0-63-generic linux-headers-5.8.0-63-generic
```

3. 开机切换到指定内核
```shell
# 确定当前内核id
grep submenu /boot/grub/grub.cfg
# 查找替换内容id
grep menuentry /boot/grub/grub.cfg
```
修改grub文件切换内核
```shell
sudo vim /etc/default/grub
```
```shell
# GRUB_DEFAULT=0 改为 GRUB_DEFAULT="当前内核id>切换内核id"，如
GRUB_DEFAULT="gnulinux-advanced-186f5d32-0b70-4366-bf7f-a7e0d5098662>gnulinux-5.8.0-63-generic-advanced-186f5d32-0b70-4366-bf7f-a7e0d5098662"
```

更新grub
```shell
sudo update-grub
reboot
```

PS: 如果启动时候内核失败（没装显卡启动等），在开机后的grub菜单选择第二项，选中对应的内核 按 E 键编辑内核启动参数:  
倒数第二行 splash 后面添加 nomodeset 然后 Ctrl+X 启动

