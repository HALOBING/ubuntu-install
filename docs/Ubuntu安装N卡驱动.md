傻瓜式安装N卡驱动
----
- 禁用自带的 nouveau nvidia 驱动
```shell
sudo gedit /etc/modprobe.d/blacklist.conf
# 末端加入
blacklist nouveau  
options nouveau modeset=0

sudo update-initramfs -u
reboot
# 验证是否禁用
lsmod | grep nouveau 
```
- 查看是否安装驱动
```shell
lspci | grep -i nvidia
# 卸载旧版本
sudo apt-get purge nvidia-*
```
- 查看显卡信息
```shell
nvidia-smi
```
```text
+-----------------------------------------------------------------------------+
| NVIDIA-SMI 450.248.02   Driver Version: 450.248.02   CUDA Version: 11.0     |
|-------------------------------+----------------------+----------------------+
| GPU  Name        Persistence-M| Bus-Id        Disp.A | Volatile Uncorr. ECC |
| Fan  Temp  Perf  Pwr:Usage/Cap|         Memory-Usage | GPU-Util  Compute M. |
|                               |                      |               MIG M. |
|===============================+======================+======================|
|   0  GeForce GT 730      Off  | 00000000:01:00.0 N/A |                  N/A |
| 28%   48C    P0    N/A /  N/A |    421MiB /  1998MiB |     N/A      Default |
|                               |                      |                  N/A |
+-------------------------------+----------------------+----------------------+
```
- 如没有需要安装
```shell
sudo add-apt-repository ppa:graphics-drivers/ppa
sudo apt update
```

查看可安装的驱动
```shell
ubuntu-drivers devices
```
```text
== /sys/devices/pci0000:00/0000:00:01.0/0000:01:00.0 ==
modalias : pci:v000010DEd00001287sv00001462sd00003380bc03sc00i00
vendor   : NVIDIA Corporation
model    : GK208B [GeForce GT 730]
driver   : nvidia-340 - distro non-free
driver   : nvidia-driver-470-server - distro non-free
driver   : nvidia-driver-390 - distro non-free
driver   : nvidia-driver-418-server - distro non-free
driver   : nvidia-driver-450-server - distro non-free
driver   : nvidia-driver-470 - distro non-free recommended
driver   : xserver-xorg-video-nouveau - distro free builtin
```
找到最适合的驱动安装，安装recommended标记的，通常也是数字版本最大的那个
```shell
sudo apt install nvidia-driver-XXX
```

也可以使用系统推荐方式安装
```shell
sudo ubuntu-drivers autoinstall
```

如果没有遇到报错，说明安装成功，此时调用nvidia-smi指令可能还是看不到显卡信息，不要担心，重启系统之后就能看到了

手动安装N卡驱动
---
这种方式有个好处，可以安装任意版本的驱动。

安装流程： 确定需要安装的驱动版本 如 450,460,470 等等, 并在<a href="https://www.nvidia.cn/Download/driverResults.aspx/172382/cn/">官网下载</a> 然后查找驱动对应的内核版本并<a href="Ubuntu切换内核版本.md">切换内核</a>
最后如果安装驱动的时候提示gcc版本不一致，需要<a href="Ubuntu编译gcc-9.3.0.md">切换gcc版本</a>到 编译内核时的版本

```shell
shdo sh NVIDIA-Linux-x86_64-460.73.01.run
```


卸载显卡驱动
---
```shell
sudo dpkg --list | grep nvidia-*
sudo apt-get remove --purge "*nvidia*"
sudo apt-get autoremove
```

可能遇到的问题
---

- 如果安装显卡后卡在 **clean **blocks界面不动如何救援：  
开机时候过了bios提示界面，后按ESC进入grub菜单（注意不要太快，1秒按一下）  
禁用Nouveasu显卡驱动： 在grub菜单界面，选中 Ubuntu 按 e 键进行编辑:  
倒数第二行 `quiet splash` 后面添加 `nomodeset` 临时修改 然后 `Ctrl + x` 启动低分辨率图像界面（tty命令行模式改为`nomodeset 3`）：
    ```shell
    sudo gedit /boot/grub/grub.cfg  
    # 搜索 `quiet splash` 后面添加 `nomodeset` 保存文本
    ```
    然后重装驱动驱动

- ERROR: An error occurred while performing the step: “Building kernel modules”. See /var/log/nvidia-installer.log for details.
可能是gcc版本过低，升级至12.3
```shell
https://mirrors.aliyun.com/gnu/gcc/gcc-12.3.0/gcc-12.3.0.tar.gz
tar -zxvf gcc-12.3.0.tar.gz
cd gcc-12.3.0
./contrib/download_prerequisites
mkdir build && cd build
../configure -enable-checking=release -enable-languages=c,c++ -disable-multilib
sudo make -j16
sudo make install
sudo ln -sf /usr/local/bin/gcc /usr/bin/gcc
sudo ln -sf /usr/local/bin/g++ /usr/bin/g++

```


