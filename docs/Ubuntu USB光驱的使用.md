实例1

```shell
growisofs -speed=2 -dvd-compat -Z /dev/dvdwriter=dvd_image.iso    #刻录光盘
```

实例2

刻录光盘语法：growisofs -dvd-compat -speed=<刻录速度> -Z <设备名>=<镜像路径>
```shell
growisofs -dvd-compat -Z /dev/dvdwriter=/opt/CentOS-7.2-x86_64-bin-DVD.iso
growisofs -dvd-compat -Z /dev/sr1=/path/to/image.iso   #刻录ISO文件

growisofs -Z /dev/sr1 -R -J /some/files    #初次刻录（非ISO文件）
growisofs -M /dev/sr1 -R -J /more/files   #往已有的DVD盘上添加文件

growisofs -M /dev/sr1=/dev/zero  #给DVD盘封口(一般用不着）
```

擦除光盘
```
dvd+rw-format -force /dev/sr0
# or
cdrecord dev=/dev/sr0 blank=fast
```