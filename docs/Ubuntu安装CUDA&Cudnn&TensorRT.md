Ubuntu下安装
---
（以下操作可能需要登录 developer.nvidia.com（67**qq Pa..12)）  

cuda 的driver API 和 runtime API:  
driver cuda version >= toolkit cuda version  


可能用到的链接：
- 查看CUDA版本与驱动版本对应关系：https://docs.nvidia.com/cuda/cuda-toolkit-release-notes/index.html
- 查看paddlepaddle-gpu各版本对CUDA版本要求： https://www.paddlepaddle.org.cn/install/old?docurl=/documentation/docs/zh/install/conda/windows-conda.html  

0. 前提
```text
(1) 安装Nvidia驱动
```

1. 查看显卡驱动支持的`CUDA`版本
```shell
nvidia-smi | grep CUDA
```
2. 安装 <a href="https://developer.nvidia.com/cuda-toolkit-archive">CUDA x.x</a>
```shell
wget https://developer.download.nvidia.com/compute/cuda/11.2.2/local_installers/cuda_11.2.2_460.32.03_linux.run
# sudo sh cuda_11.2.2_460.32.03_linux.run --toolkit --silent --override
sudo sh cuda_11.2.2_460.32.03_linux.run

# /usr/local/cuda-11.2 软连接到 /usr/local/cuda （通过 sh *.run 方式安装会自动生成该目录）
# sudo rm -rf /usr/local/cuda
# sudo ln -s /usr/local/cuda-11.2 /usr/local/cuda

sudo vim ~/.bashrc

export LD_LIBRARY_PATH="/usr/local/cuda/lib64:$LD_LIBRARY_PATH"
export PATH="/usr/local/cuda/bin:$PATH"
export CUDA_HOME="/usr/local/cuda:$CUDA_HOME"

source ~/.bashrc

# 测试安装
nvcc -V
/usr/local/cuda/extras/demo_suite/bandwidthTest # Result = PASS
/usr/local/cuda/extras/demo_suite/deviceQuery # Result = PASS
```
3. 安装 <a href="https://developer.nvidia.com/rdp/cudnn-archive">Cudnn x.x.x</a>
```shell
sudo dpkg -i libcudnn8_*.deb
sudo dpkg -i libcudnn8-dev*.deb
sudo dpkg -i libcudnn8-samples*.deb

# 查看版本

# v8
cat /usr/include/x86_64-linux-gnu/cudnn_version_v8.h | grep CUDNN_MAJOR -A 2

# 测试安装
cp -r /usr/src/cudnn_samples_v8/ $HOME # 有的版本无需拷贝直接在home目录
cd  ~/cudnn_samples_v8/mnistCUDNN
sudo make clean 
sudo make
sudo ./mnistCUDNN # Test passed!
```
4. 安装 <a href="https://developer.nvidia.com/nvidia-tensorrt-download">TensorRT8 x.x</a>, 版本需要和CUDA与Cudnn对应（看tar包下载地址 会带有Cudnn版本号 deb的没有）


```shell
# 查看是否安装旧版本
sudo dpkg --list | grep nv-tensorrt

```

5. 监控一下gpu状态
```shell
watch -n 1 nvidia-smi
```

6. 卸载cuda
```shell
sudo apt-get --purge remove "*cublas*" "cuda*"*
sudo /usr/local/cuda-11.2/bin/cuda-uninstaller
sudo apt-get --purge remove "*cublas*" "cuda*"
sudo rm -rf /usr/local/cuda /usr/local/cuda-11.2

sudo apt-get remove --purge "libcudnn8*"
sudo apt-get remove --purge "nv-tensorrt*"
```
