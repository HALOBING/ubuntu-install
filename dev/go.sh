# tar="go1.20.6.linux-amd64.tar.gz" #版本较高的在idea2021.2.2中无法debug
tar="go1.19.13.linux-amd64.tar.gz"

wget -nc https://golang.google.cn/dl/$tar -O $WORK_DIR/$tar \
 && sudo tar -xzf $WORK_DIR/$tar -C /usr/local/

# GO_LANG_HOME
sudo tee /etc/profile.d/golang.sh << EOF
# GO_LANG_HOME
export GO_LANG_HOME="/usr/local/go"
export PATH="\${PATH}:\${GO_LANG_HOME}/bin"
export GO111MODULE=on
export GOPROXY="https://goproxy.cn"
EOF