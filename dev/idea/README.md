新版本
---
1、在idea/bin/idea64.vmoptions下添加（程序已经自动添加，只需要执行下一步）
```text
-javaagent:/usr/local/ja-netfilter-all/ja-netfilter.jar=jetbrains
--add-opens=java.base/jdk.internal.org.objectweb.asm=ALL-UNNAMED
--add-opens=java.base/jdk.internal.org.objectweb.asm.tree=ALL-UNNAMED
```
2、获取激活码激活：
```
cat /usr/local/ja-netfilter-all/code.txt
```
打开idea 拷贝到Activation Code中，点击 “Activate”

老版本
---
1、以试用模式打开IDEA：选择 Evaluate for free 点击 Evaluate按钮

2、进入欢迎页面 最下方 依次点击Options Menu->Edit Coustom VM Options

3、最后一行加入
```
-javaagent:/usr/local/ja-netfilter-all/ja-netfilter.jar
```
点击Save，重启IDEA

4、进入欢迎页面 最下方 依次点击Options Menu->Manage Licenses->Add new Licenses->Activate IntelliJ IDEA->Activation Code  
获取激活码：
```
cat /usr/local/ja-netfilter-all/code.txt
```
拷贝到Activation Code中，点击 “Activate”


