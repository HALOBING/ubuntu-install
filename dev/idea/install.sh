#!/bin/bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
IDEA_VERSION=2024.1.4

wget -nc https://download.jetbrains.com/idea/ideaIU-${IDEA_VERSION}.tar.gz -O $WORK_DIR/ideaIU-${IDEA_VERSION}.tar.gz

if [ ! -d /usr/local/ja-netfilter-all ];then
  rm -rf $WORK_DIR/.config/JetBrains/IntelliJIdea* && \
    rm -rf $WORK_DIR/.cache/JetBrains/IntelliJIdea* && \
    rm -rf $WORK_DIR/.local/share/JetBrains/IntelliJIdea*

  mkdir -p /usr/local/ideaIU-${IDEA_VERSION} && \
    tar -xzf $WORK_DIR/ideaIU-${IDEA_VERSION}.tar.gz -C /usr/local/ideaIU-${IDEA_VERSION} --strip-components=1 && \
    mkdir -p /usr/local/ja-netfilter-all && \
    unzip -oq $DIR/ja-netfilter-all.zip -d /usr/local/ja-netfilter-all && \
    echo "-javaagent:/usr/local/ja-netfilter-all/ja-netfilter.jar=jetbrains" >> /usr/local/ideaIU-${IDEA_VERSION}/bin/idea64.vmoptions && \
    echo "--add-opens=java.base/jdk.internal.org.objectweb.asm=ALL-UNNAMED" >> /usr/local/ideaIU-${IDEA_VERSION}/bin/idea64.vmoptions && \
    echo "--add-opens=java.base/jdk.internal.org.objectweb.asm.tree=ALL-UNNAMED" >> /usr/local/ideaIU-${IDEA_VERSION}/bin/idea64.vmoptions

  tee $USER_DESKTOP/jetbrains-idea.desktop <<EOF
[Desktop Entry]
Version=1.0
Type=Application
Name=IntelliJ IDEA ${IDEA_VERSION}
Icon=/usr/local/ideaIU-${IDEA_VERSION}/bin/idea.svg
Exec="/usr/local/ideaIU-${IDEA_VERSION}/bin/idea.sh" %f
Comment=Capable and Ergonomic IDE for JVM
Categories=Development;IDE;
Terminal=false
StartupWMClass=jetbrains-idea
EOF
chmod 777 $USER_DESKTOP/jetbrains-idea.desktop
cp -p $USER_DESKTOP/jetbrains-idea.desktop $WORK_DIR/.local/share/applications/
chmod u+x $WORK_DIR/.local/share/applications/jetbrains-idea.desktop

git clone https://gitee.com/HALOBING/docker-idea-fcitx5.git docker-idea-fcitx5
  if [ -f "./docker-idea-fcitx5/config-idea${IDEA_VERSION}-plugins.tar.bz2.0" ]; then
    echo "Restore idea config..."
    sleep 3
    cat docker-idea-fcitx5/config-idea${IDEA_VERSION}-plugins.tar.bz2.* > config-idea${IDEA_VERSION}-plugins.tar.bz2
    tar jxvf config-idea${IDEA_VERSION}-plugins.tar.bz2
    cp -r xdg/config/JetBrains /home/${USER_NAME}/.config
    cp -r xdg/data/JetBrains /home/${USER_NAME}/.local/share
    rm -rf xdg config-idea${IDEA_VERSION}-plugins.tar.bz2
  fi

fi
    
