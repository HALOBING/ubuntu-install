#!/bin/bash
sudo systemctl daemon-reload
sudo systemctl restart docker
redis_id=$(sudo docker ps -a|grep redis | awk '{print $1}')
if [ -n "$redis_id" ] ; then
  sudo docker stop $(sudo docker ps | grep redis | awk '{print $1}') > /dev/null
  sudo docker rm $(sudo docker ps -a| grep redis | awk '{print $1}') > /dev/null
fi
sudo docker run -it --name redis --restart=always --net=host -d redis:latest redis-server > /dev/null
