#!/bin/bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

tar="apache-maven-3.6.3-bin.tar.gz"

# unzip -oq $WORK_DIR/dockerfile/jenkins/apache-maven-3.5.4-bin.zip -d /usr/local/
# wget -nc https://archive.apache.org/dist/maven/maven-3/3.6.3/binaries/$tar -O $WORK_DIR/$tar \
  # && sudo tar -xzf $WORK_DIR/$tar -C /usr/local/

sudo tar -xzf $DIR/$tar -C /usr/local/

# M2_HOME
sudo tee /etc/profile.d/maven.sh << EOF
# M2_HOME
export M2_HOME="/usr/local/apache-maven-3.6.3"
export PATH="\${PATH}:\${M2_HOME}/bin"
EOF

sudo cp -f $DIR/settings.xml /usr/local/apache-maven-3.6.3/conf/settings.xml