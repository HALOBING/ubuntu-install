#!/bin/bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

# unzip -oq $WORK_DIR/dockerfile/jdk/8u221/jdk1.8.0_221.zip -d /usr/local/
# unzip -oq $WORK_DIR/dockerfile/jdk/8u221/jre.zip -d /usr/local/jdk1.8.0_221

if [ ! -d "$WORK_DIR/jdk-8u381-linux-x64.tar.gz" ]; then
  cd $DIR && cat jdk-8u381-linux-x64.tar.* | tar -C $WORK_DIR -xvf - && cd -
fi

sudo tar -xzf $WORK_DIR/jdk-8u381-linux-x64.tar.gz -C /usr/local/

# JAVA HOME
sudo tee /etc/profile.d/java.sh << EOF

# JAVA HOME
export JAVA_HOME="/usr/local/jdk1.8.0_381"
export JRE_HOME="\${JAVA_HOME}/jre"
export PATH="\${JAVA_HOME}/bin:\$PATH"
export CLASSPATH=".:\${JAVA_HOME}/lib:\${JRE_HOME}/lib"

EOF