#!/bin/bash

# install cordova
sudo cnpm install -g cordova@8.1.2

# hot update cli
##############################################################
# 注意：
# cordova-hcp 暂时未用到ngrok模块，如果需要使用ngrok，直接替换为 ` RUN cnpm install -g cordova-hot-code-push-cli `
# 目前安装的cordova-hot-code-push-cli做了如下更改：
#   1、linux下 安装cordova-hot-code-push-cli，会远程下载ngrok二进制包，downlaod ngrok-stable-linux-amd64.zip 巨慢，
#      故将ngrok fork到https://gitee.com/HALOBING/ngrok.git并注释掉postinstall.js远程下载
#   2、将https://gitee.com/HALOBING/cordova-hot-code-push-cli.git package.json中ngrok指向https://gitee.com/HALOBING/ngrok.git
##############################################################

sudo cnpm install -g  https://gitee.com/HALOBING/cordova-hot-code-push-cli.git

# 使java_home在当前shell中生效 以安装android-sdk 在java/install.sh中设置无效
source /etc/profile.d/java.sh

#cordova -v
sudo chown -R ${USER}:${USER} ~/.config

ANDROID_HOME=/usr/local/android-sdk-linux
sudo mkdir -p $ANDROID_HOME
# install android sdk

#Warning: File /home/fuyb/.android/repositories.cfg could not be loaded.
#touch ~/.android/repositories.cfg

sudo wget -nc  https://dl.google.com/android/repository/sdk-tools-linux-3859397.zip -O ~/android-sdk-linux.zip
sudo unzip -oq ~/android-sdk-linux.zip -d $ANDROID_HOME \
    && sudo chown -R ${USER}:${USER} $ANDROID_HOME/ \
    && yes | sh $ANDROID_HOME/tools/bin/sdkmanager --licenses \
    && sh $ANDROID_HOME/tools/bin/sdkmanager "tools" \
    && sh $ANDROID_HOME/tools/bin/sdkmanager "build-tools;28.0.3" "build-tools;27.0.3" "build-tools;26.0.2" "build-tools;26.0.1" "build-tools;25.0.3" "build-tools;25.0.2" "build-tools;25.0.1" "build-tools;25.0.0" "build-tools;23.0.1" "platforms;android-28" "platforms;android-27" "platforms;android-26" "platforms;android-25" "platforms;android-23" "extras;android;m2repository" "extras;google;m2repository" --no_https \
    && sh $ANDROID_HOME/tools/bin/sdkmanager "extras;m2repository;com;android;support;constraint;constraint-layout-solver;1.0.2" "extras;m2repository;com;android;support;constraint;constraint-layout;1.0.2" --no_https \
    && sh $ANDROID_HOME/tools/bin/sdkmanager --list

# install gradle
# sudo wget -nc  https://downloads.gradle.org/distributions/gradle-5.5.1-bin.zip && \
sudo wget -nc  --no-check-certificate https://downloads.gradle-dn.com/distributions/gradle-5.6.2-bin.zip -O ~/gradle-5.6.2-bin.zip
sudo unzip -oq ~/gradle-5.6.2-bin.zip -d /usr/local/ && \
     sudo chown -R ${USER}:${USER} /usr/local/gradle-5.6.2

# ANDROID_HOME & GRADLE_HOME
sudo chown -R ${USER}:${USER} /etc/profile.d/
sudo cat >  /etc/profile.d/android_sdk.sh << EOF
# ANDROID_HOME & GRADLE_HOME
export ANDROID_HOME="$ANDROID_HOME"
export GRADLE_HOME="/usr/local/gradle-5.6.2"
export PATH="\${PATH}:\${ANDROID_HOME}/tools/bin:\${ANDROID_HOME}/tools:\${ANDROID_HOME}/platform-tools:\${GRADLE_HOME}/bin"

EOF
