#!/bin/bash

sudo tar -xzf  ~/dockerfile/tomcat/8.5/apache-tomcat-8.5.34.tar.gz -C /usr/local/ \
    && sudo chmod -R 755 /usr/local/apache-tomcat-8.5.34/

# CATALINA_HOME
sudo tee /etc/profile.d/tomcat.sh << EOF
# CATALINA_HOME
export CATALINA_HOME="/usr/local/apache-tomcat-8.5.34"
export CATALINA_BASE="\${CATALINA_HOME}"
export PATH="\${PATH}:\${CATALINA_HOME}/lib:\${CATALINA_HOME}/bin"
EOF