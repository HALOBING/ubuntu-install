#!/bin/bash

tar="dbeaver-ce-24.3.1-linux.gtk.x86_64.tar.gz"

wget -nc https://dbeaver.io/files/24.3.1/$tar -O $WORK_DIR/$tar \
 && sudo tar -xzf $WORK_DIR/$tar -C /usr/share/ \
 && mv /usr/share/dbeaver /usr/share/dbeaver-ce \
 && chmod u+x /usr/share/dbeaver-ce/dbeaver-ce.desktop \
 && cp /usr/share/dbeaver-ce/dbeaver-ce.desktop /usr/share/applications/ \
 && cp /usr/share/dbeaver-ce/dbeaver-ce.desktop $USER_DESKTOP/